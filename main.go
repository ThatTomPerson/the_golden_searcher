package main

import (
	"fmt"
  "golang.org/x/exp/mmap"
	"path/filepath"
	"os"
	"flag"
)

func matches(filename string) {
	var text string = "ipsum"

	finder := makeStringFinder(text)

	file, _ := mmap.Open(filename)
	var counter int
	idx := -1
	for {
		idx = finder.next(file, idx + 1)
		fmt.Printf("%d\n", idx)

		if idx == -1 {
			break
		}

		counter = counter + 1
	}

	fmt.Printf("searching %s and found %d\n", text, counter)
}

func visit(path string, f os.FileInfo, err error) error {
	if !f.IsDir() {
		go matches(path)
	}

	fmt.Printf("Visited: %s\n", path)
	return nil
}


func main() {
	flag.Parse()
	root := "./scratch"
	err := filepath.Walk(root, visit)
	fmt.Printf("filepath.Walk() returned %v\n", err)
}